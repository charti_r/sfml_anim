#ifndef UTIL_HPP_
# define UTIL_HPP_

# include <string>
# include <sstream>

namespace Util
{
  template <typename T>
  std::string convertNbtoA(T nb)
  {
    std::stringstream ss;

    ss << nb;
    return ss.str();
  }

  template <typename T>
  T convertAtoNb(const std::string & nb)
  {
    std::stringstream ss(nb);
    T ret;

    ss >> ret;
    return ret;
  }

  template <typename T>
  T convertAtoNb(char * nb)
  {
    std::stringstream ss(nb);
    T ret;

    ss >> ret;
    return ret;
  }
}

#endif // !UTIL_HPP_
