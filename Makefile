CPPFLAGS	+= -W -Wall -Werror -Wextra -o3

LIB		= -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system

NAME		= animtest

SRC		= \
		main.cpp \
		Anim.cpp

OBJ		= $(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJ)
	g++ $(CPPFLAGS) $(LIB) $(OBJ) -o $(NAME)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
