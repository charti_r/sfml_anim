#include "Anim.hpp"

namespace mysf
{
  Anim::Anim()
    : Transformable()
    , _texture(0)
    , _textureRect(0, 0, 0, 0)
    , _offset(0, 0)
    , _vector(1, 0)
    , _sprites(1, std::vector<sf::Sprite>(1))
    , _size(1, 1)
    , _nbMiss(0)
    , _speed(sf::seconds(1.f))
    , _animated(true)
    , _state(0)
  {

  }

  Anim::Anim(const sf::Texture & texture)
    : Transformable()
    , _texture(&texture)
    , _textureRect(0, 0, _texture->getSize().x, _texture->getSize().y)
    , _offset(0, 0)
    , _vector(1, 0)
    , _sprites(1, std::vector<sf::Sprite>(1))
    , _size(1, 1)
    , _nbMiss(0)
    , _speed(sf::seconds(1.f))
    , _animated(true)
    , _state(0)
  {

  }

  Anim::Anim(const sf::Texture & texture, const sf::IntRect & rectangle)
    : Transformable()
    , _texture(&texture)
    , _textureRect(rectangle)
    , _offset(0, 0)
    , _vector(1, 0)
    , _sprites(1, std::vector<sf::Sprite>(1))
    , _size(1, 1)
    , _nbMiss(0)
    , _speed(sf::seconds(1.f))
    , _animated(true)
    , _state(0)
  {

  }

  Anim::Anim(const Anim & o)
    : Transformable(o)
    , _texture(o._texture)
    , _textureRect(o._textureRect)
    , _offset(o._offset)
    , _vector(o._vector)
    , _sprites(o._sprites)
    , _size(o._size)
    , _nbMiss(o._nbMiss)
    , _speed(o._speed)
    , _animated(o._animated)
    , _state(o._state)
  {

  }

  Anim & Anim::operator=(const Anim & o)
  {
    if (&o == this)
      return *this;
    Drawable::operator=(o);
    Transformable::operator=(o);
    _texture = o._texture;
    _textureRect = o._textureRect;
    _offset = o._offset;
    _vector = o._vector;
    _sprites = o._sprites;
    _size = o._size;
    _nbMiss = o._nbMiss;
    _speed = o._speed;
    _animated = o._animated;
    _state = o._state;
    return *this;
  }

  Anim::~Anim()
  {

  }

  void Anim::setTexture(const sf::Texture & texture)
  {
    _texture = &texture;
    _textureRect.left = 0;
    _textureRect.top = 0;
    _textureRect.width = _texture->getSize().x;
    _textureRect.height = _texture->getSize().y;
  }

  void Anim::setTexture(const sf::Texture & texture, const sf::IntRect & rectangle)
  {
    _texture = &texture;
    _textureRect = rectangle;
  }

  void Anim::setTextureRect(const sf::IntRect & rectangle)
  {
    _textureRect = rectangle;
  }

  void Anim::setVector(const sf::Vector2i & vector)
  {
    if (vector.x ? !vector.y : vector.y) // x XOR y
      _vector = vector;
  }

  void Anim::setVector(int x, int y)
  {
    if (x ? !y : y) // x XOR y
      _vector = sf::Vector2i(x, y);
  }

  void Anim::setSize(const sf::Vector2u & size, unsigned int nbMiss, const sf::Vector2i & offset)
  {
    if (_size != size)
      {
	_size = size;
	_sprites.resize(size.x);
	for (unsigned int x = 0; x < size.x; ++x)
	  _sprites[x].resize(size.y);
      }
    _nbMiss = nbMiss;
    _offset = offset;
  }

  void Anim::setSpeed(const sf::Time & speed)
  {
    _speed = speed;
  }

  void Anim::reset(bool play)
  {
    _animated = play;
    _state = 0;
  }

  void Anim::play()
  {
    _animated = true;
  }

  void Anim::pause()
  {
    _animated = false;
  }

  void Anim::build()
  {
    sf::Vector2i spriteSize;

    spriteSize.x = (_textureRect.width - (_offset.x * (_size.x ? _size.x - 1 : 0))) / (_size.x ? _size.x : 1);
    spriteSize.y = (_textureRect.height - (_offset.y * (_size.y ? _size.y - 1 : 0))) / (_size.y ? _size.y : 1);
    for (unsigned int x = 0; x < _size.x; ++x)
      for (unsigned int y = 0; y < _size.y; ++y)
	{
	  sf::Vector2i pos(_textureRect.left, _textureRect.top);

	  pos.x += (_vector.x == 0 ? (y * spriteSize.x) + (y * _offset.x) :
		    _vector.x == 1 ? (x * spriteSize.x) + (x * _offset.x) :
		    (_textureRect.width - ((x + 1) * spriteSize.x)) + ((_size.x - x) * _offset.x));

	  pos.y += (_vector.y == 0 ? (y * spriteSize.y) + (y * _offset.y) :
		    _vector.y == 1 ? (x * spriteSize.y) + (x * _offset.y) :
		    (_textureRect.height - ((x + 1) * spriteSize.y)) + ((_size.y - x) * _offset.y));

	  _sprites[x][y].setTexture(*_texture);
	  _sprites[x][y].setTextureRect(sf::IntRect(pos, spriteSize));
	}
  }

  const sf::Texture & Anim::getTexture() const
  {
    return *_texture;
  }

  const sf::IntRect & Anim::getRect() const
  {
    return _textureRect;
  }

  const sf::Vector2i & Anim::getVector() const
  {
    return _vector;
  }

  unsigned int Anim::getSize() const
  {
    return _sprites.size();
  }

  const sf::Time & Anim::getSpeed() const
  {
    return _speed;
  }

  const std::vector<sf::Sprite> & Anim::operator[](unsigned int index) const
  {
    return _sprites[index];
  }

  void Anim::draw(sf::RenderTarget & target, sf::RenderStates states) const
  {
    static sf::Time elapsed;
    static sf::Clock clock;

    elapsed += clock.restart();
    if (_animated)
      while (elapsed > _speed)
	{
	  _state = (_state + 1) % ((_size.x * _size.y) - _nbMiss);
	  elapsed -= _speed;
	}
    else
      elapsed = sf::Time::Zero;
    target.draw(_sprites[_state % _size.x][_state / _size.x], states);
  }
}
