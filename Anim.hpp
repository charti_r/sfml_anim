#ifndef ANIM_HPP_
# define ANIM_HPP_

# include <vector>
# include <fstream>

# include <SFML/Graphics.hpp>
# include <SFML/System.hpp>

namespace mysf
{
  class Anim : public sf::Drawable, public sf::Transformable
  {
  public:
    Anim();
    explicit Anim(const sf::Texture & texture);
    Anim(const sf::Texture & texture, const sf::IntRect & rectangle);
    Anim(const Anim & o);
    Anim & operator=(const Anim & o);
    virtual ~Anim();

    void		setTexture(const sf::Texture & texture);
    void		setTexture(const sf::Texture & texture, const sf::IntRect & rectangle);
    void		setTextureRect(const sf::IntRect & rectangle);
    void		setVector(const sf::Vector2i & vector);
    void		setVector(int x, int y);
    void		setSize(const sf::Vector2u & size, unsigned int nbMiss = 0,
				const sf::Vector2i & offset = sf::Vector2i(0, 0));
    void		setSpeed(const sf::Time & speed);
    void		reset(bool play = true);
    void		play();
    void		pause();
    void		build();

    const sf::Texture &			getTexture() const;
    const sf::IntRect &			getRect() const;
    const sf::Vector2i &		getVector() const;
    unsigned int			getSize() const;
    const sf::Time &			getSpeed() const;

    const std::vector<sf::Sprite> &	operator[](unsigned int index) const;

    virtual void	draw(sf::RenderTarget & target, sf::RenderStates states) const;

  private:
    const sf::Texture *		_texture;
    sf::IntRect			_textureRect;
    sf::Vector2i		_offset;
    sf::Vector2i		_vector;
    std::vector<std::vector<sf::Sprite> >	_sprites;
    sf::Vector2u		_size;
    unsigned int		_nbMiss;
    sf::Time			_speed;
    bool			_animated;
    mutable unsigned int	_state;
  };
} // !mysf

#endif // !ANIM_HPP_
