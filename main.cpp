#include <iostream>

#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include "Util.hpp"

#include "Anim.hpp"

int main(int ac, char ** av)
{
  sf::RenderWindow win(sf::VideoMode(900, 600), "SFML Anim Test");
  sf::Event event;
  sf::Texture texture;
  mysf::Anim anim;

  if (texture.loadFromFile("circles.png") == false)
    return 1;
  texture.setSmooth(true);

  anim.setTexture(texture);
  anim.setVector(1, 0);
  anim.setSize(sf::Vector2u(8, 8), 4);
  anim.setSpeed(sf::seconds(ac >= 2 ? Util::convertAtoNb<float>(av[1]) : 1.f));
  anim.build();

  win.setView(sf::View(sf::FloatRect(0.f, 0.f, 60.f, 60.f)));
  while (win.isOpen())
    {
      while (win.pollEvent(event))
	{
	  switch (event.type)
	    {
	    case sf::Event::Closed:
	      win.close();
	      break;
	    default:
	      break;
	    }
	}
      win.clear();
      anim.draw(win, sf::RenderStates::Default);
      win.display();
    }

  return 0;
}
